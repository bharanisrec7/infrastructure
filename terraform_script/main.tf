terraform {
  backend "http" {
  }
}

module "vpc" {
  source = "./modules/vpc"
}


module "iam" {
  source = "./modules/iam"
}


module "ec2" {
  source = "./modules/ec2"
  public_subnet_id = module.vpc.public_subnet_id
  public_security_group = module.vpc.public_security_group_id
  iam_role_name = module.iam.iam_role_name
  runner_token = var.runner_token
}

module "postgres" {
  source = "./modules/postgres"
  username = var.username
  password = var.password
  dbname = var.dbname
  s3_bucket_name = var.s3_bucket_name
  private_subnet_id = module.vpc.private_subnet_id
  private_security_group_id = module.vpc.private_security_group_id
}

module "s3" {
  source = "./modules/s3"
  bucket_name = var.s3_bucket_name
}
