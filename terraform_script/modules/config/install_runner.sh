#! /bin/bash
sudo yum install docker -y
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo yum install git -y
sudo curl -L --output /tmp/gitlab-runner_amd64.rpm "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"
sudo rpm -ivh /tmp/gitlab-runner_amd64.rpm
sudo gitlab-runner register -n \
  -u https://gitlab.com/ \
  -r "${key}" \
  --executor docker \
  --tag-list python-app-docker \
  --name python-app-docker-runner \
  --docker-hostname python-app-docker-runner \
  --output-limit 100000 \
  --locked="true" \
  --docker-image="docker:19.03.13" \
  --docker-volumes="/var/run/docker.sock:/var/run/docker.sock"