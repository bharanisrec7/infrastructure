data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = [local.ami_id]
  }

  owners = [local.owners] 
}

resource "aws_instance" "this" {
  ami              = data.aws_ami.amazon_linux.id
  instance_type    = local.instance_type
  user_data        = templatefile("./modules/config/install_runner.sh", {
      key = var.runner_token
  })
  subnet_id = var.public_subnet_id
  key_name               = local.key_name
  vpc_security_group_ids = [var.public_security_group]
  iam_instance_profile   = local.iam_instance_profile
  associate_public_ip_address = local.associate_public_ip_address

  ebs_block_device {
      delete_on_termination = local.delete_on_termination
      device_name           = local.device_name
      iops                  = local.iops
      volume_size           = local.volume_size
      volume_type           = local.volume_type
  }

  tags = {
      environment = "production"
  }
}