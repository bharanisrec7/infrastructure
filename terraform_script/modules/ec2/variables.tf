variable "public_subnet_id" {

}

variable "public_security_group" {    

}

variable "iam_role_name" {

}


variable "runner_token" {
    
}

locals {
    instance_type = "t2.micro"
    owners = "amazon"
    key_name = "MyVPCKey"
    associate_public_ip_address = true
    iam_instance_profile = var.iam_role_name
    delete_on_termination =  true
    device_name = "/dev/xvda"
    iops = "100"
    volume_size = 8
    volume_type = "gp2"
    ami_id = "amzn-ami-hvm-*-x86_64-gp2"

}