resource "aws_iam_role" "python_iam_role" {
  name = local.iam_role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    name= "python-iam-role"
  }
}

resource "aws_iam_instance_profile" "test_profile" {
  name = "python_app_role"
  role = aws_iam_role.python_iam_role.name
}

resource "aws_iam_policy" "policy" {
  name        = local.iam_policy_name
  path        = "/"
  description = "My python app policy"
  policy = file("./modules/config/python_app_policy.json")
}

resource "aws_iam_role_policy_attachment" "python_policy_attachment" {
  role       = aws_iam_role.python_iam_role.name
  policy_arn = aws_iam_policy.policy.arn
}