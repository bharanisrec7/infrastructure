variable "username" {

}

variable "password" {

}

variable "dbname" {

}

variable "s3_bucket_name" {

}

variable "private_subnet_id" {

}

variable "private_security_group_id" {

}

locals {
  db_credentials = {
    "/production/database/username/master" = var.username
    "/production/database/password/master" = var.password
    "/production/database/dbname/master" = var.dbname
    "/production/s3/bucketname" = var.s3_bucket_name
  }

}