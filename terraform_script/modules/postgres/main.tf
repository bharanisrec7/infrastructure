resource "aws_ssm_parameter" "secret" {
  for_each = local.db_credentials
  name        = each.key
  description = "master db pwd s3"
  type        = "SecureString"
  value       = each.value

  tags = {
    environment = "production"
  }
}


resource "aws_db_subnet_group" "private_subnet_group" {
  name       = "private_subnet_group"
  subnet_ids = var.private_subnet_id

  tags = {
    Name = "My DB private subnet group"
  }
}

resource "aws_db_instance" "mydb1" {
  db_subnet_group_name = aws_db_subnet_group.private_subnet_group.name
  allocated_storage        = 256 # gigabytes
  backup_retention_period  = 7   # in days
  engine                   = "postgres"
  engine_version           = "10.4"
  identifier               = "mydb1"
  instance_class           = "db.t2.micro"
  multi_az                 = false
  name                     = var.dbname
  password                 = var.password
  port                     = 5432
  publicly_accessible      = false
  storage_encrypted        = false
  skip_final_snapshot      = true
  storage_type             = "gp2"
  username                 = var.username
  vpc_security_group_ids   = [var.private_security_group_id]
}

resource "aws_ssm_parameter" "rds_host_secret" {
  name        = "/production/database/dbhost/master"
  description = "master db endpoint"
  type        = "SecureString"
  value       = aws_db_instance.mydb1.endpoint

  tags = {
    environment = "production"
  }
  depends_on=[aws_db_instance.mydb1]
}

