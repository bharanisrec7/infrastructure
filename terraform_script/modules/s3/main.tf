data "aws_iam_policy_document" "website_policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [
      "arn:aws:s3:::${var.bucket_name}/index.html"
    ]
  }
}

resource "aws_s3_bucket" "b" {
  bucket = var.bucket_name
  acl = "public-read"
  policy = data.aws_iam_policy_document.website_policy.json
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  tags = {
    Name        = "My bucket"
    Environment = "Prod"
  }
}
