resource "aws_vpc" "main" {
  cidr_block       = local.vpc_cidr_block
  instance_tenancy = "default"

  tags = {
    Name = "python_vpc"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "python_gw"
  }
}

resource "aws_route_table" "r" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = local.route_cidr_block
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "python_rt"
  }
}


resource "aws_subnet" "sn1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = local.subnet_cidr_block_sn1
  availability_zone = "us-east-1a"

  tags = {
    Name = "python_sn1"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.sn1.id
  route_table_id = aws_route_table.r.id
}

resource "aws_subnet" "sn2" {
  for_each = local.private_subnets
  vpc_id     = aws_vpc.main.id
  cidr_block = each.value
  availability_zone = each.key

  tags = {
    Name = "python_sn2"
  }
}


resource "aws_security_group" "sn1-sg" {
  name        = "sn1-sg"
  description = "sn1-sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sn1-sg"
  }
}


resource "aws_security_group" "sn2-sg" {
  name        = "sn2-sg"
  description = "sn2-sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "POSTGRES"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.main.cidr_block]
  }


  tags = {
    Name = "sn2-sg"
  }
}