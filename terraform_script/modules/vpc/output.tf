output "public_subnet_id" {
    value = aws_subnet.sn1.id
}

output "public_security_group_id" {
    value = aws_security_group.sn1-sg.id
}

output "private_subnet_id" {
    value = [ for i in aws_subnet.sn2 : i.id ]
}


output "private_security_group_id" {
    value = aws_security_group.sn2-sg.id
}