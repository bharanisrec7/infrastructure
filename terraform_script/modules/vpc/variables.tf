locals {
  vpc_cidr_block = "10.0.0.0/16"
  route_cidr_block = "0.0.0.0/0"
  subnet_cidr_block_sn1 = "10.0.0.0/24"
  private_subnets = {
     "us-east-1b" = "10.0.1.0/24"
     "us-east-1a" = "10.0.2.0/24"
  }
}